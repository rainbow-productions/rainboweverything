// Constants
// const CLIENT_ID = 'xTXbsht8Zd9bQhcl';
const CLIENT_ID = 'kXNicAtcdDkdv6kZ'
const adminPassword = "jcpsAL5988"; // Add your desired admin password here

// Variables
let choosename = prompt("Choose your name");
let isAdmin = false;
let kickedMemberId = null;

// Prompt for admin password when the admin button is clicked
document.getElementById("admin").addEventListener("click", function() {
    const password = prompt("Enter admin password");
    if (password === adminPassword) {
        isAdmin = true;
        alert("Admin mode enabled!");
    } else {
        alert("Incorrect admin password!");
    }
});

// Request notification permission
Notification.requestPermission();

// Initialize Scaledrone
const drone = new ScaleDrone(CLIENT_ID, {
    data: {
        name: choosename,
        color: getRandomColor(),
    },
});

alert("Welcome, " + choosename + "!");

let members = [];

// Scaledrone event handlers
drone.on('open', error => {
    if (error) {
        return console.error(error);
    }
    console.log('Successfully connected to Scaledrone');

    const room = drone.subscribe('observable-room');
    room.on('open', error => {
        if (error) {
            return console.error(error);
        }
        console.log('Successfully joined room');
    });

    room.on('members', m => {
        members = m;
        updateMembersDOM();
    });

    room.on('member_join', member => {
        members.push(member);
        updateMembersDOM();
    });

    room.on('member_leave', ({ id }) => {
        const index = members.findIndex(member => member.id === id);
        members.splice(index, 1);
        updateMembersDOM();
    });

    room.on('data', (text, member) => {
        if (member) {
            addMessageToListDOM(text, member);
        } else {
            // Message is from server
        }
    });
});

drone.on('close', event => {
    console.log('Connection was closed', event);
});

drone.on('error', error => {
    console.error(error);
});

// Function to generate a random color
function getRandomColor() {
    return '#' + Math.floor(Math.random() * 0xFFFFFF).toString(16);
}

// DOM elements
const DOM = {
    membersCount: document.querySelector('.members-count'),
    membersList: document.querySelector('.members-list'),
    messages: document.querySelector('.messages'),
    input: document.querySelector('.message-form__input'),
    form: document.querySelector('.message-form'),
};

// Event listener for sending messages
DOM.form.addEventListener('submit', sendMessage);

function sendMessage() {
    const value = DOM.input.value;
    if (value === '') {
        return;
    }
    DOM.input.value = '';
    drone.publish({
        room: 'observable-room',
        message: value,
    });
}

// ... (previous code)

// Create a DOM element for a member
function createMemberElement(member) {
  const { name, color, id } = member.clientData;
  const el = document.createElement('div');

  if (isAdmin) {
      // Add the ability to change colors and names if in admin mode
      const colorInput = document.createElement('input');
      colorInput.type = "color";
      colorInput.value = color;
      colorInput.addEventListener('change', function() {
          member.clientData.color = this.value;
          el.style.color = this.value;
      });

      const nameInput = document.createElement('input');
      nameInput.type = "text";
      nameInput.value = name;
      nameInput.addEventListener('change', function() {
          member.clientData.name = this.value;
          el.firstChild.textContent = this.value;
      });

      el.appendChild(nameInput);
      el.appendChild(colorInput);
  } else {
      // If not in admin mode, display member name
      el.appendChild(document.createTextNode(name));
  }

  el.className = 'member';
  el.style.color = color;

  // Store the member's ID for future reference
  el.dataset.memberId = id;

  // Attach click event to handle kicks (only for admins)
  if (isAdmin) {
      el.style.cursor = 'pointer'; // Change cursor to pointer for admins
      el.addEventListener("click", function(event) {
          event.preventDefault(); // Prevent default link behavior
          event.stopPropagation(); // Prevent the click event from propagating to the admin's click handler
          handleKick(id);
      });
  }

  return el;
}

// Event listener for kicking a member
function handleKick(memberId) {
  const confirmKick = confirm("Kick this member from the chat?");
  if (confirmKick) {
      // Publish a special message to the kicked member before redirecting
      drone.publish({
          room: 'observable-room',
          message: 'You have been kicked from the chat.',
          to: memberId, // Send the message only to the kicked member
      });

      // Redirect the kicked member to "kicked.html"
      kickedMemberId.window.location.href = "kicked.html";
  }
}


// Update the members list in the DOM
function updateMembersDOM() {
    DOM.membersCount.innerText = `${members.length} users in room:`;
    DOM.membersList.innerHTML = '';
    members.forEach(member =>
        DOM.membersList.appendChild(createMemberElement(member))
    );
}

// Create a DOM element for a message
function createMessageElement(text, member) {
    const el = document.createElement('div');
    const memberEl = createMemberElement(member);

    if (isAdmin) {
        // Only allow kicking when in admin mode
        memberEl.addEventListener("click", function() {
            const confirmKick = confirm("Kick " + member.clientData.name + " from the chat?");
            if (confirmKick) {
                kickedMemberId = memberEl.dataset.memberId;
                window.location.href = "kicked.html";
            }
        });
    }

    el.appendChild(memberEl);
    el.appendChild(document.createTextNode(text));
    el.className = 'message';

    return el;
}

// Add a message to the messages list in the DOM
function addMessageToListDOM(text, member) {
    const el = DOM.messages;
    const wasTop = el.scrollTop === el.scrollHeight - el.clientHeight;
    const notificationOptions = {
        body: 'New message from ' + member.clientData.name + ': ' + text
    };

    el.appendChild(createMessageElement(text, member));

    // Play the notification sound
    const notificationSound = document.getElementById('notificationSound');
    notificationSound.play();

    // Create and display the notification
    let membersElements = el.getElementsByClassName('member');
    let messageElements = el.getElementsByClassName('message');
    new Notification('RainbowChat', notificationOptions);

    if (wasTop) {
        el.scrollTop = el.scrollHeight - el.clientHeight;
    }
}
