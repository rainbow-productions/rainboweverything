function checkIPBan() {
    var bannedIPs = ['37.9.144.0', '']; // Add your list of banned IP addresses here
  
    // Make a request to the ipify API to retrieve the client's IP address
    fetch('https://api.ipify.org/?format=json')
      .then(response => response.json())
      .then(data => {
        var ipAddress = data.ip;
  
        // Perform the IP ban check
        if (bannedIPs.includes(ipAddress)) {
          // If the IP is in the banned IP list, treat it as banned
          alert('Error: Access Denied for ' + ipAddress + ' Nice try.');
          window.location.href = 'ip-banned.html';
        } else {
          // If not banned, continue with the code
          window.alert('Clear')
        }
      })
      .catch(error => {
        console.log('An error occurred while retrieving the IP address: ', error);
      });
  }
  
  // Call the function to check IP ban
  checkIPBan();
  