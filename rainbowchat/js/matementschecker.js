var isDown = false;
var downtime = '0:00';

if (isDown) {
    console.log('Maintenance is ongoing.');
    alert('Maintenance is currently ongoing. Expected uptime: ' + downtime);
    window.location.href = 'maintenance.html'; // Corrected the syntax for redirection
} else {
    console.log('Maintenance is not ongoing.');
}
